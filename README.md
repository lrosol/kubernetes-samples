# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains sample kubernetes deployment for simple web application.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Build spring-boot-webapp

```bash
./gradlew :services:springboot-webapp:bootJar
```

* Build docker image

```bash
docker build -f docker/services/springboot-webapp/Dockerfile --build-arg JAR_FILE=services/springboot-webapp/build/li
bs/*.jar -t pro.lrosol/springboot-webapp
```

* Run app in container
```bash
docker run -d -p 8080:8080 pro.lrosol/springboot-webapp
```


* Kubernetes
To run this service in kubernetes cluster it is required to install kubernetes in the system.
https://minikube.sigs.k8s.io/docs/start/

```bash
minikube start --driver=docker
minikube dashboard
```

```bash
http://127.0.0.1:51633/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
```

### Gradle ###
List of projects:
```bash
./gradlew -q projects
```

### Documentation

https://projectcontour.io/docs/v1.20.1/config/fundamentals/
https://konghq.com/blog/kong-gateway-oauth2
https://docs.docker.com/compose/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact ###

* lukaszrosol@gmail.com