# how to run
execute run:
```bash
./run.sh
```

run container from final image
```bash
docker run -it --rm --entrypoint sh chunk:2
```

print merged file
```bash
cat /tmp/file
```

# cleanup
```bash
./cleanup.sh
```

# links
https://ostechnix.com/split-combine-files-command-line-linux/
