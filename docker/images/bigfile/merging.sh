#!/bin/bash

for line in `cat latest`; do
JAR_TYPE=$(echo $line | cut -d "|" -f 1)

if ! grep -q $JAR_TYPE res; then
  echo "Not found version for $JAR_TYPE"
fi

done
